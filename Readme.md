#Assignment
####Each student should define the features for an app that will support, at least, the following:
        - an input form
        - a list of items
        - display a chart
        - authentication
        - offline support - persist data on the local storage
        - online support - synchronize date to/from a remote location
        - intent - eg. show map coordinates on google maps, or send an email using gmail
        - animations

#Planner app
#### A planning app where you can do the following:
        - login + register
        - offline/ online support
        - email notifications
        - statistics
        - view most recent meetings
        - premium users