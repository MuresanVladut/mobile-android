package planner.com.planner.rest;

import java.util.List;

import planner.com.planner.model.ToDo;

public class ToDoResponse {
    private List<ToDo> todos;

    public List<ToDo> getTodos() {
        return todos;
    }

    public void setTodos(List<ToDo> todos) {
        this.todos = todos;
    }
}
