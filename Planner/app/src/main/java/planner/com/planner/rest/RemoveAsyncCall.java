package planner.com.planner.rest;

import android.os.AsyncTask;

import com.squareup.okhttp.ResponseBody;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;


public class RemoveAsyncCall extends AsyncTask<Call, Void, Void> {
    @Override
    protected Void doInBackground(Call... params) {
        try {
            Call<Void> call = params[0];
            call.execute();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
