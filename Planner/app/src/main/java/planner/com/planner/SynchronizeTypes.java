package planner.com.planner;


public enum SynchronizeTypes {
    ADD,
    REMOVE,
    EDIT
}
