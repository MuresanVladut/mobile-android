package planner.com.planner.model;

import java.util.Date;

public class ToDo {
    private String title;
    private boolean completed;
    private Date createdAt;
    private Integer id;

    public ToDo(String title, boolean completed, Date createdAt) {
        this.title = title;
        this.completed = completed;
        this.createdAt = createdAt;
    }

    public ToDo() {

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public String toString() {
        return "ToDo{" +
                "title='" + title + '\'' +
                ", completed=" + completed +
                ", createdAt=" + createdAt +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
