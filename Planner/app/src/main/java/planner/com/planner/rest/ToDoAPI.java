package planner.com.planner.rest;

import com.squareup.okhttp.ResponseBody;

import java.util.List;

import planner.com.planner.model.ToDo;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ToDoAPI {
    @GET("/todos")
    Call<List<ToDo>> getAll();

    @DELETE("/todos/{id}")
    Call<Void> deleteToDo(@Path("id") Integer id);

    @POST("/todos")
    Call<ToDo> addToDo(@Body ToDo toDo);

    @PUT("/todos/{id}")
    Call<ToDo> editToDo(@Path("id") Integer id, @Body ToDo toDo);
}
