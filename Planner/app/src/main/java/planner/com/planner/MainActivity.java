package planner.com.planner;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.auth0.core.UserProfile;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;
import com.squareup.okhttp.ResponseBody;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import planner.com.planner.model.ToDo;
import planner.com.planner.rest.AddAsyncCall;
import planner.com.planner.rest.EditAsyncCall;
import planner.com.planner.rest.RemoveAsyncCall;
import planner.com.planner.rest.ServiceGenerator;
import planner.com.planner.rest.ToDoAPI;
import retrofit2.Call;


public class MainActivity extends ActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks, NetworkStateReceiver.NetworkStateReceiverListener {

    //Create Objects.
    private ListView myList;
    private ListAdapter todoListAdapter;
    private TodoListSQLHelper todoListSQLHelper;
    private NetworkStateReceiver networkStateReceiver;
    private boolean isConnected = true;


    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;
    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    private void sendEmail(UserProfile userProfile) {
        Log.i("Send email", "");
        Intent emailIntent = new Intent(Intent.ACTION_SEND);

        todoListSQLHelper = new TodoListSQLHelper(MainActivity.this);
        List<ToDo> stringList = todoListSQLHelper.getAllTodos();

        emailIntent.setData(Uri.parse("mailto:muresan25@gmail.com"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, userProfile.getEmail());
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{userProfile.getEmail()});
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Planner Login");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Your ToDo List: \n " + stringList.toString());

        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            finish();
        } catch (ActivityNotFoundException ex) {
            Toast.makeText(MainActivity.this, "There is no email client installed.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myList = (ListView) findViewById(R.id.list);
        ImageButton fabImageButton = (ImageButton) findViewById(R.id.fab_image_button);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));

        //listener that watch for the network change
        networkStateReceiver = new NetworkStateReceiver();
        networkStateReceiver.addListener(this);
        this.registerReceiver(networkStateReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));


        final ArrayList<ToDo> list = new ArrayList<>();
        final MyCustomAdapter adapter = new MyCustomAdapter(this, list);
        SwipeDismissListViewTouchListener touchListener =
                new SwipeDismissListViewTouchListener(
                        (ListView) findViewById(R.id.list),
                        new SwipeDismissListViewTouchListener.DismissCallbacks() {
                            @Override
                            public boolean canDismiss(int position) {
                                return true;
                            }

                            @Override
                            public void onDismiss(ListView listView, int[] reverseSortedPositions) {
                                for (int position : reverseSortedPositions) {
                                    Long id = todoListAdapter.getItemId(position);
                                    String deleteTodoItemSql = "DELETE FROM " + TodoListSQLHelper.TABLE_NAME +
                                            " WHERE " + TodoListSQLHelper._ID + " = '" + id + "'";

                                    todoListSQLHelper = new TodoListSQLHelper(MainActivity.this);
                                    SQLiteDatabase sqlDB = todoListSQLHelper.getWritableDatabase();
                                    sqlDB.execSQL(deleteTodoItemSql);
                                    updateTodoList();
                                    if(isConnected){
                                        ToDoAPI service = ServiceGenerator.createService(ToDoAPI.class);
                                        Call<Void> all = service.deleteToDo(id.intValue());
                                        new RemoveAsyncCall().execute(all);
                                    }
                                    else {
                                        ContentValues values = new ContentValues();
                                        values.clear();
                                        values.put(TodoListSQLHelper.COL1_TASK_SYNC, SynchronizeTypes.REMOVE.toString());
                                        values.put(TodoListSQLHelper.COL2_TASK_SYNC, id);
                                        sqlDB.insertWithOnConflict(TodoListSQLHelper.TABLE_NAME_SYNC, null, values, SQLiteDatabase.CONFLICT_IGNORE);
                                        System.out.println("List= " + todoListSQLHelper.getAllSimple());
                                    }
                                }
                            }

                        });
        findViewById(R.id.list).setOnTouchListener(touchListener);
        // Setting this scroll listener is required to ensure that during ListView scrolling,
        // we don't look for swipes.
        // findViewById(R.id.list).setOnScrollListener(touchListener.makeScrollListener());

        fabImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapter.notifyDataSetChanged();
                AlertDialog.Builder todoTaskBuilder = new AlertDialog.Builder(MainActivity.this);
                todoTaskBuilder.setTitle("Add a List item.");
                todoTaskBuilder.setMessage("Describe the item.");
                final EditText todoET = new EditText(MainActivity.this);
                todoTaskBuilder.setView(todoET);
                todoTaskBuilder.setPositiveButton("Add Item", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String todoTaskInput = todoET.getText().toString();
                        todoListSQLHelper = new TodoListSQLHelper(MainActivity.this);
                        SQLiteDatabase sqLiteDatabase = todoListSQLHelper.getWritableDatabase();
                        ContentValues values = new ContentValues();
                        values.clear();

                        //write the Todo task input into database table
                        values.put(TodoListSQLHelper.COL1_TASK, todoTaskInput);
                        values.put(TodoListSQLHelper.COL2_TASK, false);
                        Date date = new Date();
                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
                        values.put(TodoListSQLHelper.COL3_TASK, formatter.format(date).toString());
                        Long id = sqLiteDatabase.insertWithOnConflict(TodoListSQLHelper.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_IGNORE);

                        System.out.println("ID= " + id);
                        //update the Todo task list UI
                        updateTodoList();
                        if(isConnected){
                            ToDoAPI service = ServiceGenerator.createService(ToDoAPI.class);
                            Call<ToDo> doCall = service.addToDo(todoListSQLHelper.getToDo(id.intValue()));
                            new AddAsyncCall().execute(doCall);
                        } else {
                            values.clear();
                            values.put(TodoListSQLHelper.COL1_TASK_SYNC, SynchronizeTypes.ADD.toString());
                            values.put(TodoListSQLHelper.COL2_TASK_SYNC, id);
                            sqLiteDatabase.insertWithOnConflict(TodoListSQLHelper.TABLE_NAME_SYNC, null, values, SQLiteDatabase.CONFLICT_IGNORE);
                            System.out.println("List= " + todoListSQLHelper.getAllSimple());
                        }
                    }
                });

                todoTaskBuilder.setNegativeButton("Cancel", null);

                todoTaskBuilder.create().show();
            }
        });

        //show the ListView on the screen
        // The adapter MyCustomAdapter is responsible for maintaining the data backing this list and for producing
        // a view to represent an item in that data set.

        updateTodoList();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();
        switch (position) {
            case 0:
                break;
            case 1:
                Intent intent = new Intent(this, Chart.class);
                startActivity(intent);
                break;
        }
    }

    public void onSectionAttached(int number) {
        switch (number) {
            case 1:
                mTitle = getString(R.string.title_section1);
                break;
            case 2:
                mTitle = getString(R.string.title_section2);
                break;
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        //noinspection SimplifiableIfStatement
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            ArrayList<ToDo> list = new ArrayList<>();

            MyCustomAdapter adapter = new MyCustomAdapter(this, list);

            adapter.notifyDataSetChanged();
            AlertDialog.Builder todoTaskBuilder = new AlertDialog.Builder(MainActivity.this);
            todoTaskBuilder.setTitle("Add List Item.");
            todoTaskBuilder.setMessage("Describe the item.");
            final EditText todoET = new EditText(MainActivity.this);
            todoTaskBuilder.setView(todoET);
            todoTaskBuilder.setPositiveButton("Add Task", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    String todoTaskInput = todoET.getText().toString();
                    todoListSQLHelper = new TodoListSQLHelper(MainActivity.this);
                    SQLiteDatabase sqLiteDatabase = todoListSQLHelper.getWritableDatabase();
                    ContentValues values = new ContentValues();
                    values.clear();

                    values.put(TodoListSQLHelper.COL1_TASK, todoTaskInput);
                    values.put(TodoListSQLHelper.COL2_TASK, false);
                    Date date = new Date();
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
                    values.put(TodoListSQLHelper.COL3_TASK, formatter.format(date).toString());
                    Long id = sqLiteDatabase.insertWithOnConflict(TodoListSQLHelper.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_IGNORE);

                    //update the Todo task list UI
                    updateTodoList();
                    if(isConnected){
                        ToDoAPI service = ServiceGenerator.createService(ToDoAPI.class);
                        Call<ToDo> doCall = service.addToDo(todoListSQLHelper.getToDo(id.intValue()));
                        new AddAsyncCall().execute(doCall);
                    } else{
                        values.clear();
                        values.put(TodoListSQLHelper.COL1_TASK_SYNC, SynchronizeTypes.ADD.toString());
                        values.put(TodoListSQLHelper.COL2_TASK_SYNC, id);
                        sqLiteDatabase.insertWithOnConflict(TodoListSQLHelper.TABLE_NAME_SYNC, null, values, SQLiteDatabase.CONFLICT_IGNORE);
                        System.out.println("List= " + todoListSQLHelper.getAllSimple());
                    }
                    sqLiteDatabase.close();


                }
            });

            todoTaskBuilder.setNegativeButton("Cancel", null);

            todoTaskBuilder.create().show();


            //show the ListView on the screen
            // The adapter MyCustomAdapter is responsible for maintaining the data backing this list and for producing
            // a view to represent an item in that data set.

            updateTodoList();
            return true;

        }
        if (id == R.id.send_mail) {
            UserProfile logged = getIntent().getExtras().getParcelable("userprofile");
            sendEmail(logged);
        }
        return super.onOptionsItemSelected(item);

    }

    private void updateTodoList() {
        todoListSQLHelper = new TodoListSQLHelper(MainActivity.this);
        SQLiteDatabase sqLiteDatabase = todoListSQLHelper.getReadableDatabase();

        //cursor to read todo task list from database
        Cursor cursor = sqLiteDatabase.query(TodoListSQLHelper.TABLE_NAME,
                new String[]{TodoListSQLHelper._ID, TodoListSQLHelper.COL1_TASK},
                null, null, null, null, null);

        //binds the todo task list with the UI
        todoListAdapter = new SimpleCursorAdapter(
                this,
                R.layout.due,
                cursor,
                new String[]{TodoListSQLHelper.COL1_TASK},
                new int[]{R.id.due_text_view},
                0
        );

        myList.setAdapter(todoListAdapter);
    }

    public void onDoneButtonClick(View view){
        View v = (View) view.getParent();
        ListView listView = (ListView) v.getParent();
        final int position = listView.getPositionForView(v);
        final Long itemId = todoListAdapter.getItemId(position);
        CheckBox viewById = (CheckBox) findViewById(R.id.done_button);

        todoListSQLHelper = new TodoListSQLHelper(MainActivity.this);
        SQLiteDatabase sqLiteDatabase = todoListSQLHelper.getWritableDatabase();
        int checked = viewById.isChecked() ? 1 : 0;
        String editTodoItemSql = "UPDATE " + TodoListSQLHelper.TABLE_NAME +
                " SET " + TodoListSQLHelper.COL2_TASK + " = '" + checked + "'" +
                "WHERE " + TodoListSQLHelper._ID + " = '" + itemId + "'";
        sqLiteDatabase.execSQL(editTodoItemSql);
        if(isConnected){
            ToDo toDo = todoListSQLHelper.getToDo(itemId.intValue());
            toDo.setCompleted(viewById.isChecked());
            ToDoAPI service = ServiceGenerator.createService(ToDoAPI.class);
            Call<ToDo> editCall = service.editToDo(itemId.intValue(), toDo);
            new EditAsyncCall().execute(editCall);
        } else{
            ContentValues values = new ContentValues();
            values.clear();
            values.put(TodoListSQLHelper.COL1_TASK_SYNC, SynchronizeTypes.EDIT.toString());
            values.put(TodoListSQLHelper.COL2_TASK_SYNC, itemId);
            sqLiteDatabase.insertWithOnConflict(TodoListSQLHelper.TABLE_NAME_SYNC, null, values, SQLiteDatabase.CONFLICT_IGNORE);
        }
        sqLiteDatabase.close();

    }

    //closing the todo task item
    public void onEditButtonClick(View view) {
        final ArrayList<ToDo> list = new ArrayList<>();
        final MyCustomAdapter adapter = new MyCustomAdapter(this, list);
        View v = (View) view.getParent();
        TextView todoTV = (TextView) v.findViewById(R.id.due_text_view);
        final String todoTaskItem = todoTV.getText().toString();

        ListView listView = (ListView) v.getParent();
        final int position = listView.getPositionForView(v);
        final Long itemId = todoListAdapter.getItemId(position);


        adapter.notifyDataSetChanged();
        AlertDialog.Builder todoTaskBuilder = new AlertDialog.Builder(MainActivity.this);
        todoTaskBuilder.setTitle("Edit the selected item.");
        todoTaskBuilder.setMessage("Describe the item.");
        final EditText todoET = new EditText(MainActivity.this);
        todoET.setText(todoTaskItem);
        todoTaskBuilder.setView(todoET);
        todoTaskBuilder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                todoListSQLHelper = new TodoListSQLHelper(MainActivity.this);
                SQLiteDatabase sqLiteDatabase = todoListSQLHelper.getWritableDatabase();

                String editTodoItemSql = "UPDATE " + TodoListSQLHelper.TABLE_NAME +
                        " SET " + TodoListSQLHelper.COL1_TASK + " = '" + todoET.getText().toString() + "'" +
                        "WHERE " + TodoListSQLHelper._ID + " = '" + itemId + "'";
                sqLiteDatabase.execSQL(editTodoItemSql);
                updateTodoList();
                if(isConnected){
                    ToDoAPI service = ServiceGenerator.createService(ToDoAPI.class);
                    Call<ToDo> editCall = service.editToDo(itemId.intValue(), todoListSQLHelper.getToDo(itemId.intValue()));
                    new EditAsyncCall().execute(editCall);
                } else{
                    ContentValues values = new ContentValues();
                    values.clear();
                    values.put(TodoListSQLHelper.COL1_TASK_SYNC, SynchronizeTypes.EDIT.toString());
                    values.put(TodoListSQLHelper.COL2_TASK_SYNC, itemId);
                    sqLiteDatabase.insertWithOnConflict(TodoListSQLHelper.TABLE_NAME_SYNC, null, values, SQLiteDatabase.CONFLICT_IGNORE);
                    System.out.println("List= " + todoListSQLHelper.getAllSimple());
                }
                sqLiteDatabase.close();
            }
        });

        todoTaskBuilder.setNegativeButton("Cancel", null);

        todoTaskBuilder.create().show();
    }

    @Override
    public void networkAvailable() {
        isConnected = true;
        List<Pair<SynchronizeTypes, Integer>> allSimple = todoListSQLHelper.getAllSimple();
        ToDoAPI service = ServiceGenerator.createService(ToDoAPI.class);
        for (Pair<SynchronizeTypes,Integer> pair : allSimple){
                if(pair.first.equals(SynchronizeTypes.ADD)){
                    Call<ToDo> addCall = service.addToDo(todoListSQLHelper.getToDo(pair.second));
                    new AddAsyncCall().execute(addCall);
                    todoListSQLHelper.removePair(pair.first, pair.second);
                }
                if(pair.first.equals(SynchronizeTypes.REMOVE)){
                    Call<Void> all = service.deleteToDo(pair.second);
                    new RemoveAsyncCall().execute(all);
                    todoListSQLHelper.removePair(pair.first, pair.second);
                }
                if(pair.first.equals(SynchronizeTypes.EDIT)){
                    Call<ToDo> editCall = service.editToDo(pair.second, todoListSQLHelper.getToDo(pair.second));
                    new EditAsyncCall().execute(editCall);
                    todoListSQLHelper.removePair(pair.first, pair.second);
                }
        }
    }

    @Override
    public void networkUnavailable() {
        isConnected = false;
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("Main Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }

    @Override
    protected void onDestroy() {
        this.unregisterReceiver(networkStateReceiver);
    }
}
