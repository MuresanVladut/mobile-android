package planner.com.planner;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.text.TextUtils;
import android.util.Pair;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import planner.com.planner.model.ToDo;

public class TodoListSQLHelper extends SQLiteOpenHelper {

    public static final String DB_NAME = "chaos.list.db";
    public static final String TABLE_NAME = "LIST";
    public static final String COL1_TASK = "title";
    public static final String COL2_TASK = "done";
    public static final String COL3_TASK = "createdAt";
    public static final String _ID = BaseColumns._ID;

    public static final String TABLE_NAME_SYNC = "SYNCHRONIZE";
    public static final String COL1_TASK_SYNC = "action";
    public static final String COL2_TASK_SYNC = "LIST_ID";

    public TodoListSQLHelper(Context context) {
        //1 is todo list database version
        super(context, DB_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqlDB) {
        String createTodoListTable = "CREATE TABLE " + TABLE_NAME + " ( _id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COL1_TASK + " TEXT, " + COL2_TASK + " BOOLEAN NOT NULL CHECK(" + COL2_TASK +" IN (0,1)), "
                +COL3_TASK + " DATETIME"
                + ")";
        String createSyncTable = "CREATE TABLE " + TABLE_NAME_SYNC + " ( _id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COL1_TASK_SYNC + " TEXT, " + COL2_TASK_SYNC + " INTEGER"
                + ")";
        sqlDB.execSQL(createTodoListTable);
        sqlDB.execSQL(createSyncTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqlDB, int i, int i2) {
        sqlDB.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        sqlDB.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_SYNC);
        onCreate(sqlDB);
    }

    public List<ToDo> getAllTodos() {
        List<ToDo> list = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + TodoListSQLHelper.TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        try {
            Cursor cursor = db.rawQuery(selectQuery, null);
            try {
                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        ToDo toDo = new ToDo();
                        toDo.setTitle(cursor.getString(1));
                        toDo.setCompleted(cursor.getInt(2) != 0);
                        String oldstring = cursor.getString(3);
                        Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").parse(oldstring);
                        toDo.setCreatedAt(date);
                        list.add(toDo);
                    } while (cursor.moveToNext());
                }
            } catch (ParseException e) {
                e.printStackTrace();
            } finally {
                try {
                    cursor.close();
                } catch (Exception ignore) {
                }
            }
        } finally {
            try {
                db.close();
            } catch (Exception ignore) {
            }
        }
        return list;
    }

    public List<Pair<SynchronizeTypes, Integer>> getAllSimple() {
        List<Pair<SynchronizeTypes,Integer>> list = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + TodoListSQLHelper.TABLE_NAME_SYNC;
        SQLiteDatabase db = this.getReadableDatabase();
        try {
            Cursor cursor = db.rawQuery(selectQuery, null);
            try {
                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        list.add(new Pair<>(SynchronizeTypes.valueOf(cursor.getString(1)), cursor.getInt(2)));
                    } while (cursor.moveToNext());
                }
            } finally {
                try {
                    cursor.close();
                } catch (Exception ignore) {
                }
            }
        } finally {
            try {
                db.close();
            } catch (Exception ignore) {
            }
        }
        return list;
    }


    public ToDo getToDo(Integer id) {
        ToDo toDo = new ToDo();
        String selectQuery = "SELECT  * FROM " + TodoListSQLHelper.TABLE_NAME + " WHERE "
                + TodoListSQLHelper._ID + "=" + id;
        SQLiteDatabase db = this.getReadableDatabase();
        try {
            Cursor cursor = db.rawQuery(selectQuery, null);
            try {
                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        toDo.setId(cursor.getInt(0));
                        toDo.setTitle(cursor.getString(1));
                        toDo.setCompleted(cursor.getInt(2) != 0);
                        String oldstring = cursor.getString(3);
                        Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").parse(oldstring);
                        toDo.setCreatedAt(date);
                    } while (cursor.moveToNext());
                }
            } catch (ParseException e) {
                e.printStackTrace();
            } finally {
                try {
                    cursor.close();
                } catch (Exception ignore) {
                }
            }
        } finally {
            try {
                db.close();
            } catch (Exception ignore) {
            }
        }
        return toDo;
    }

    public void removePair(SynchronizeTypes synchronizeTypes, Integer id) {
        String type = "\'" + synchronizeTypes + "\'";
        String selectQuery = "DELETE FROM " + TodoListSQLHelper.TABLE_NAME_SYNC + " WHERE "
                + COL1_TASK_SYNC + "=" + type  +" AND " + COL2_TASK_SYNC + "=" + id;
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(selectQuery);
    }
}
