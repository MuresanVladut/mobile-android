package planner.com.planner;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;
import java.util.List;

import planner.com.planner.model.ToDo;

public class Chart extends AppCompatActivity implements NavigationDrawerFragment.NavigationDrawerCallbacks {
    private TodoListSQLHelper todoListSQLHelper = new TodoListSQLHelper(Chart.this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        LineChart lineChart = (LineChart) findViewById(R.id.chart);
        int[] array = new int[12];
        List<ToDo> data = todoListSQLHelper.getAllTodos();
        for(ToDo toDo: data){
            switch (toDo.getCreatedAt().getMonth()){
                case 0:
                    array[0]++;
                    break;
                case 1:
                    array[1]++;
                    break;
                case 2:
                    array[2]++;
                    break;
                case 3:
                    array[3]++;
                    break;
                case 4:
                    array[4]++;
                    break;
                case 5:
                    array[5]++;
                    break;
                case 6:
                    array[6]++;
                    break;
                case 7:
                    array[7]++;
                    break;
                case 8:
                    array[8]++;
                    break;
                case 9:
                    array[9]++;
                    break;
                case 10:
                    array[10]++;
                    break;
                case 11:
                    array[11]++;
                    break;
            }
        }


        ArrayList<Entry> entries = new ArrayList<>();
        for(int i = 0; i<12;i++){
            entries.add(new Entry(array[i],i));
        }

        LineDataSet dataset = new LineDataSet(entries, "# of ToDos");

        ArrayList<String> labels = new ArrayList<>();
        labels.add("January");
        labels.add("February");
        labels.add("March");
        labels.add("April");
        labels.add("May");
        labels.add("June");
        labels.add("July");
        labels.add("August");
        labels.add("September");
        labels.add("October");
        labels.add("November");
        labels.add("December");



        LineData chart = new LineData(labels, dataset);
        dataset.setColors(ColorTemplate.COLORFUL_COLORS); //
        dataset.setDrawCubic(true);
        dataset.setDrawFilled(true);

        lineChart.setData(chart);
        lineChart.animateY(50000);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_about, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        switch (position) {
            case 0:
            case 1:
                Intent intent = new Intent(this, Chart.class);
                startActivity(intent);
                break;
        }
    }
}
