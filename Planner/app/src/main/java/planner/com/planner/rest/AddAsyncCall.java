package planner.com.planner.rest;

import android.os.AsyncTask;
import android.support.v7.widget.LinearLayoutCompat;

import java.io.IOException;
import java.util.List;

import planner.com.planner.model.ToDo;
import retrofit2.Call;
import retrofit2.Response;


public class AddAsyncCall extends AsyncTask<Call,Void,ToDo>{
    @Override
    protected ToDo doInBackground(Call... params) {
        try {
            Call<ToDo> call = params[0];
            Response<ToDo> execute = call.execute();
            return execute.body();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(ToDo todo){
        System.out.printf("After ADD= " + todo);
    }
}
